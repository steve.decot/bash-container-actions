#!/bin/bash
##
echo "---------------------------------------------------------------------------------------------"
echo "Collecte des compose.yml tournant sur cet host, aucunes actions autre que GET n est effectuee"
echo "---------------------------------------------------------------------------------------------"
##
idsDocker=`docker ps --format '{{.ID}}'`

for id in $idsDocker;
do
# container=$(docker container inspect $id --format '{{ index .Config.Labels "com.docker.compose.project.working_dir" }}')
container=$(docker container inspect $id --format '{{ index .Config.Labels "com.docker.compose.project.config_files" }}')
workindir+=($container)
done

workindir_uniq=`printf '%s\n' ${workindir[@]} | sort -u `
date_bkp=`date +'%Y%m%d'`
dir_bkp="/var/backups/docker/"
# On a tous les dossiers dans un array
cpt=1
for wk in $workindir_uniq;
do
        bwk=`basename $wk`
        #echo "Fichier yml $cpt : `ls -l $wk/docker-compose.yml`"
        echo "Fichier yml $cpt : `ls -l $wk`"
	((cpt=cpt +1))
done
echo "-------------------------------------------------------------------------"
echo "il y a `expr $cpt - 1` docker-compose au total qui tournent sur cet host" 
echo "-------------------------------------------------------------------------"