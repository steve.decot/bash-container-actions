# bash-container-actions
## Description
Ce repo contiendra une liste de script bash qui permettra diverses actions sur des compose.yml tournant sur vos hosts. 

### list-container.sh
Liste les compose.yml tournant sur votre host. 

#### Pré-requis

* docker
* docker-compose
* chmod +x sur le bash
#### Sortie d'exemple
```bash 
---------------------------------------------------------------------------------------------
Collecte des compose.yml tournant sur cet host, aucunes actions autre que GET n est effectuee
---------------------------------------------------------------------------------------------
Fichier yml 1 : -rw-r--r-- 1 root root 1607 Oct 25 20:26 /docker/zone-1/app-site-1/docker-compose.yml
Fichier yml 3 : -rw-r--r-- 1 root root 2269 Dec 30 00:24 /docker/private/app-domain-10/docker-compose.yml
Fichier yml 4 : -rw-r----- 1 root root 1451 Nov 24 21:14 /docker/test/side-project-test/try-compose.yml
Fichier yml 5 : -rw-r--r-- 1 root root 1585 Nov 12 20:43 /docker/dev/alpha-project/dev.yml
Fichier yml 6 : -rw-r--r-- 1 root root 1458 Oct 25 20:29 /docker/prod/what/power.yml
Fichier yml 7 : -rw-r--r-- 1 root root 1908 Dec 11 20:58 /docker/prod/powerfull/amazing.yml
-------------------------------------------------------------------------
il y a 6 docker-compose au total qui tournent sur cet host
-------------------------------------------------------------------------
```

### Customisation 
Afin d'aller plus loin, installez le paquage ```jq``` sur votre host
Ensuite récupérez l'id d'un container via 

```bash 
docker ps 
```
Ensuite, affectez cet id dans une variable ```id```
Exécutez la commande suivante

```bash 
docker container inspect $id --format '{{json . }}' | jq
```

Vous pourrez chercher le(s) info(s) que vous souhaitez avoir. Il n'y a plus qu'à les ajouter dans le script 

## Futures features
ajout d'autre script : start / stop / restart ... 

## Author 
Steve Decot 